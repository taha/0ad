Source: 0ad
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Vincent Cheng <vcheng@debian.org>,
 Ludovic Rousseau <rousseau@debian.org>
Build-Depends:
 autoconf,
 debhelper-compat (= 12),
 dpkg-dev (>= 1.15.5),
 libboost-dev,
 libboost-filesystem-dev,
 libcurl4-gnutls-dev | libcurl4-dev,
 libenet-dev (>= 1.3),
 libgloox-dev (>= 1.0.10),
 libicu-dev,
 libminiupnpc-dev (>= 1.6),
 libnspr4-dev,
 libnvtt-dev (>= 2.0.8-1+dfsg-4~),
 libogg-dev,
 libopenal-dev,
 libpng-dev,
 libsdl2-dev (>= 2.0.2),
 libsodium-dev (>= 1.0.14),
 libvorbis-dev,
 libwxgtk3.0-gtk3-dev,
 libxcursor-dev,
 libxml2-dev,
 pkg-config,
 python2,
 python3,
 zlib1g-dev
Standards-Version: 4.5.0
Homepage: http://play0ad.com/
Vcs-Git: https://salsa.debian.org/games-team/0ad.git
Vcs-Browser: https://salsa.debian.org/games-team/0ad
Rules-Requires-Root: no

Package: 0ad
Architecture: amd64 arm64 armhf i386 kfreebsd-amd64 kfreebsd-i386
Pre-Depends: dpkg (>= 1.15.6~)
Depends:
 0ad-data (>= ${source:Upstream-Version}),
 0ad-data (<= ${source:Version}),
 0ad-data-common (>= ${source:Upstream-Version}),
 0ad-data-common (<= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Description: Real-time strategy game of ancient warfare
 0 A.D. (pronounced "zero ey-dee") is a free, open-source, cross-platform
 real-time strategy (RTS) game of ancient warfare. In short, it is a
 historically-based war/economy game that allows players to relive or rewrite
 the history of Western civilizations, focusing on the years between 500 B.C.
 and 500 A.D. The project is highly ambitious, involving state-of-the-art 3D
 graphics, detailed artwork, sound, and a flexible and powerful custom-built
 game engine.
